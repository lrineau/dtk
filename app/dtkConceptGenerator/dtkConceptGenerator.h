// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtWidgets>

class dtkConceptGenerator : public QWizard
{
public:
    dtkConceptGenerator(void);

public:
    void accept(void);
};

//
// dtkConceptGenerator.h ends here
