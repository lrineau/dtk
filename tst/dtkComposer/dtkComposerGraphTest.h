/* @(#)dtkComposerGraphTest.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2016 - Nicolas Niclausse, Inria.
 * Created: 2016/08/17 14:32:38
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <dtkTest>

class dtkComposerGraph;
class dtkComposerNodeFactory;
class dtkComposerReaderNoScene;

class dtkComposerGraphTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testReaderFor(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkComposerNodeFactory *m_factory;
    dtkComposerGraph *m_graph;
    dtkComposerReaderNoScene *m_reader;

};



