// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDistributedBufferManagerTest.h"

#include <dtkDistributed>
#include <dtkDistributedTest>

void dtkDistributedBufferManagerTestCase::initTestCase(void)
{
    dtkDistributedSettings settings;
    settings.beginGroup("communicator");
    dtkDistributed::communicator::initialize(settings.value("plugins").toString());
    settings.endGroup();

}

void dtkDistributedBufferManagerTestCase::init(void)
{

}

void dtkDistributedBufferManagerTestCase::testAll(void)
{
    communicator_buffermanager_test::runAll("qthread");
}

void dtkDistributedBufferManagerTestCase::cleanupTestCase(void)
{
    dtkDistributed::communicator::pluginManager().uninitialize();
}

void dtkDistributedBufferManagerTestCase::cleanup(void)
{

}

DTKDISTRIBUTEDTEST_MAIN_NOGUI(dtkDistributedBufferManagerTest, dtkDistributedBufferManagerTestCase)

//
// dtkDistributedBufferManagerTest.cpp ends here
