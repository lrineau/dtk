// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

class dtkDistributedBufferManagerTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testAll(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

};

//
// dtkDistributedBufferManagerTest.h ends here
