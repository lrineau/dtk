// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#define DTKCOREPLUGINFACTORYTEST_EXPORT

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkCorePluginFactoryTestConcept
{
public:
    virtual ~dtkCorePluginFactoryTestConcept(void) {}

public:
    virtual QString name(void) const = 0;
};

DTK_DECLARE_OBJECT(dtkCorePluginFactoryTestConcept *)
DTK_DECLARE_PLUGIN_FACTORY(dtkCorePluginFactoryTestConcept, DTKCOREPLUGINFACTORYTEST_EXPORT)

//
// dtkCorePluginFactoryTestConcept.h ends here
