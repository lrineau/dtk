// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkObjectManagerTest.h"

#include <QtTest>

#include <dtkCore>
#include <dtkDistributed>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkObjectManagerTestCasePrivate
{
public:
    dtkDistributedCommunicator *comm_1;
};

dtkObjectManagerTestCase::dtkObjectManagerTestCase(void): d(new dtkObjectManagerTestCasePrivate)
{
    d->comm_1 = nullptr;
}

dtkObjectManagerTestCase::~dtkObjectManagerTestCase(void)
{

}

void dtkObjectManagerTestCase::initTestCase(void)
{

    QMetaType::registerComparators<dtkDistributedCommunicator*>();
    dtkDistributed::communicator::pluginFactory().connect(dtkObjectManager::instance());

    dtkDistributed::communicator::initialize();
    d->comm_1 = dtkDistributed::communicator::pluginFactory().create("qthread");
    dtkDistributedCommunicator *communicator_2 = dtkDistributed::communicator::pluginFactory().create("qthread");

    QVERIFY(d->comm_1 != nullptr);
    QVERIFY(communicator_2 != nullptr);
}

void dtkObjectManagerTestCase::init(void)
{

}

void dtkObjectManagerTestCase::testCount(void)
{
    QCOMPARE(dtkObjectManager::instance()->count() , 2);
}

void dtkObjectManagerTestCase::testValue(void)
{
    QVariant v = dtkObjectManager::instance()->value(QString("dtkDistributedCommunicator* 0"));
    dtkDistributedCommunicator * comm = v.value<dtkDistributedCommunicator*>();
    QCOMPARE(comm, d->comm_1);
}

void dtkObjectManagerTestCase::testKeys(void)
{
    QCOMPARE(dtkObjectManager::instance()->keys()[0] , QString("dtkDistributedCommunicator* 0"));
    QCOMPARE(dtkObjectManager::instance()->keys()[1] , QString("dtkDistributedCommunicator* 1"));
}

void dtkObjectManagerTestCase::testDel(void)
{
    QVariant v;
    QCOMPARE(dtkObjectManager::instance()->remove(v), false);
    v.setValue(d->comm_1);
    QCOMPARE(dtkObjectManager::instance()->remove(v), true);
    QCOMPARE(dtkObjectManager::instance()->count(), 1);

    v = dtkObjectManager::instance()->value(QString("dtkDistributedCommunicator* 1"));
    dtkDistributedCommunicator * comm = v.value<dtkDistributedCommunicator*>();
    QCOMPARE(dtkObjectManager::instance()->remove(v), true);
    QCOMPARE(dtkObjectManager::instance()->count() , 0);
    QCOMPARE(dtkObjectManager::instance()->remove(v), false);
    QCOMPARE(dtkObjectManager::instance()->count() , 0);
}

void dtkObjectManagerTestCase::cleanup(void)
{

}

void dtkObjectManagerTestCase::cleanupTestCase(void)
{

}

DTKTEST_MAIN_NOGUI(dtkObjectManagerTest, dtkObjectManagerTestCase)

//
// dtkObjectManagerTest.cpp ends here
