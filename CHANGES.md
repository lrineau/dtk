# ChangeLog
## version 1.4.2 - 2018-03-14
- fix dtkConfig when installing
## version 1.4.1 - 2018-03-09
- fix cmake for dtkWrap when dtk is used as a submodule
## version 1.4.0 - 2018-03-08
- major update to python wrapping: enhance swig (now we can implement pure python plugins) + add sip wrapping for dtkWidgets. update to dtk_wrap macro API
- no longer use find_packages for Qt in dtkConfig
- add dtkObjectManager
- add dtkViewManager and associated classes
- add dtkCorePluginWidgetManager as a singleton that stores a map between concept objects encapsulated within a QVariant and widgets that have to be provided by the plugins. This enable plugins that have specific setters and getters to be handled in the composer through widget editors. This mechanism replace dtkWidgetFactory
- minor bugfixes
- **this will be the last version of DTK that will include the support layers **
