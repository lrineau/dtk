## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/qt_core.i   DESTINATION wrp/QtCore/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/qrunnable.i DESTINATION wrp/QtCore/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/QVariant.i  DESTINATION wrp/QtCore/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/QObject.h   DESTINATION wrp/QtCore/)

######################################################################
### CMakeLists.txt ends here
