// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

class QBoxLayout : public QLayout
{
public:
    enum Direction {
        LeftToRight	= 0,
        RightToLeft = 1,
        TopToBottom = 2,
        BottomToTop = 3
    };

public:
    QBoxLayout(Direction, QWidget *);
};

//
// QBoxLayout.h ends here
