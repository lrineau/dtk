// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

class QLayout : public QObject
{
public:
    virtual int	count(void) const = 0;
};

//
// QLayout.h ends here
