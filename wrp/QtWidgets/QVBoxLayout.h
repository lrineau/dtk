// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

class QVBoxLayout : public QBoxLayout
{
public:
    QVBoxLayout(void);
    QVBoxLayout(QWidget *parent);
};

//
// QVBoxLayout.h ends here
