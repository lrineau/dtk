## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkComposer)

if(DTK_BUILD_COMPOSER AND NOT DTK_BUILD_WIDGETS)
  message(SEND_ERROR "You can't select \"DTK_BUILD_COMPOSER\" because it's dependencies (DTK_BUILD_WIDGETS) were not met")
endif()

if(DTK_BUILD_COMPOSER AND NOT DTK_BUILD_DISTRIBUTED)
  message(SEND_ERROR "You can't select \"DTK_BUILD_COMPOSER\" because it's dependencies (DTK_BUILD_DISTRIBUTED) were not met")
endif()

## #################################################################
## Input
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkComposer
  dtkComposer.h
  dtkComposerCompass
  dtkComposerCompass.h
  dtkComposerControls
  dtkComposerControls.h
  dtkComposerControlsDelegate
  dtkComposerControlsDelegate.h
  dtkComposerControlsListItem.h
  dtkComposerControlsListItemFactory.h
  dtkComposerControlsListItemBoolean.h
  dtkComposerControlsListItemFile.h
  dtkComposerControlsListItemInteger.h
  dtkComposerControlsListItemReal.h
  dtkComposerControlsListItemString.h
  dtkComposerDefaultExtensions.h
  dtkComposerEvaluator
  dtkComposerEvaluator.h
  dtkComposerEvaluator_p
  dtkComposerEvaluator_p.h
  dtkComposerEvaluatorProcess.h
  dtkComposerEvaluatorSlave.h
  dtkComposerEvaluatorToolBar.h
  dtkComposerExtension.h
  dtkComposerGraph
  dtkComposerGraph.h
  dtkComposerGraphEdge
  dtkComposerGraphEdge.h
  dtkComposerGraphNode
  dtkComposerGraphNode.h
  dtkComposerGraphNodeBegin
  dtkComposerGraphNodeBegin.h
  dtkComposerGraphNodeBeginForEach.h
  dtkComposerGraphNodeEnd
  dtkComposerGraphNodeEnd.h
  dtkComposerGraphNode.h
  dtkComposerGraphNodeLeaf
  dtkComposerGraphNodeLeaf.h
  dtkComposerGraphNodeSelectBranch
  dtkComposerGraphNodeSelectBranch.h
  dtkComposerGraphNodeSetInputs
  dtkComposerGraphNodeSetInputs.h
  dtkComposerGraphNodeSetOutputs
  dtkComposerGraphNodeSetOutputs.h
  dtkComposerGraphNodeSetVariables
  dtkComposerGraphNodeSetVariables.h
  dtkComposerGraphView
  dtkComposerGraphView.h
  dtkComposerMetaType.h
  dtkComposerNodeGraph
  dtkComposerNodeGraph.h
  dtkComposerNodeGraphEdge
  dtkComposerNodeGraphEdge.h
  dtkComposerNode
  dtkComposerNode.h
  dtkComposerNodeObject
  dtkComposerNodeObject.h
  dtkComposerNodeObject.tpp
  dtkComposerNodeMetaData.h
  dtkComposerNodeDistributed
  dtkComposerNodeDistributed.h
  dtkComposerNodeFactory.h
  dtkComposerNodeFactoryView.h
  dtkComposerNodeBoolean
  dtkComposerNodeBoolean.h
  dtkComposerNodeBoolean_p.h
  dtkComposerNodeBooleanOperator
  dtkComposerNodeBooleanOperator.h
  dtkComposerNodeComposite
  dtkComposerNodeComposite.h
  dtkComposerNodeConstants
  dtkComposerNodeConstants.h
  dtkComposerNodeControl
  dtkComposerNodeControl.h
  dtkComposerNodeControlCase
  dtkComposerNodeControlCase.h
  dtkComposerNodeControlDoWhile
  dtkComposerNodeControlDoWhile.h
  dtkComposerNodeControlIf
  dtkComposerNodeControlIf.h
  dtkComposerNodeControlFor
  dtkComposerNodeControlFor.h
  dtkComposerNodeControlForEach
  dtkComposerNodeControlForEach.h
  dtkComposerNodeControlMap
  dtkComposerNodeControlMap.h
  dtkComposerNodeControlParallelMap.h
  dtkComposerNodeControlWhile
  dtkComposerNodeControlWhile.h
  dtkComposerNodeExec.h
  dtkComposerNodeFile
  dtkComposerNodeFile.h
  dtkComposerNodeFile_p
  dtkComposerNodeFile_p.h
  dtkComposerNodeInteger
  dtkComposerNodeInteger.h
  dtkComposerNodeLeaf
  dtkComposerNodeLeaf.h
  dtkComposerNodeRange.h
  dtkComposerNodeLogger
  dtkComposerNodeLogger.h
  dtkComposerNodeMetaContainer.h
  dtkComposerNodeNumberOperator
  dtkComposerNodeNumberOperator.h
  dtkComposerNodePrint.h
  dtkComposerNodeProxy
  dtkComposerNodeProxy.h
  dtkComposerNodeReal
  dtkComposerNodeReal.h
  dtkComposerNodeRemote
  dtkComposerNodeRemote.h
  dtkComposerNodeSpawn
  dtkComposerNodeSpawn.h
  dtkComposerNodeSpawnEmbedded
  dtkComposerNodeSpawnEmbedded.h
  dtkComposerNodeString
  dtkComposerNodeString.h
  dtkComposerNodeStringOperator
  dtkComposerNodeStringOperator.h
  dtkComposerPath
  dtkComposerPath.h
  dtkComposerScene
  dtkComposerScene.h
  dtkComposerScene_p
  dtkComposerScene_p.h
  dtkComposerSceneEdge
  dtkComposerSceneEdge.h
  dtkComposerSceneModel
  dtkComposerSceneModel.h
  dtkComposerSceneNode
  dtkComposerSceneNode.h
  dtkComposerSceneNode_p
  dtkComposerSceneNode_p.h
  dtkComposerSceneNodeComposite
  dtkComposerSceneNodeComposite.h
  dtkComposerSceneNodeControl
  dtkComposerSceneNodeControl.h
  dtkComposerSceneNodeHandle
  dtkComposerSceneNodeHandle.h
  dtkComposerSceneNodeEditor
  dtkComposerSceneNodeEditor.h
  dtkComposerSceneNodeEditor_p
  dtkComposerSceneNodeEditor_p.h
  dtkComposerSceneNodeLeaf
  dtkComposerSceneNodeLeaf.h
  dtkComposerSceneNote
  dtkComposerSceneNote.h
  dtkComposerScenePort
  dtkComposerScenePort.h
  dtkComposerSceneView
  dtkComposerSceneView.h
  dtkComposerSearchDialog
  dtkComposerSearchDialog.h
  dtkComposerSettings.h
  dtkComposerStack
  dtkComposerStack.h
  dtkComposerStackCommand
  dtkComposerStackCommand.h
  dtkComposerStackUtils
  dtkComposerStackUtils.h
  dtkComposerStackView
  dtkComposerStackView.h
  dtkComposerTransmitter
  dtkComposerTransmitter.h
  dtkComposerTransmitter_p
  dtkComposerTransmitter_p.h
  dtkComposerTransmitterEmitter
  dtkComposerTransmitterEmitter.h
  dtkComposerTransmitterEmitter.tpp
  dtkComposerTransmitterHandler.h
  dtkComposerTransmitterHandler.tpp
  dtkComposerTransmitterProxy
  dtkComposerTransmitterProxy.h
  dtkComposerTransmitterProxyLoop.h
  dtkComposerTransmitterProxyLoop.tpp
  dtkComposerTransmitterProxyVariant.h
  dtkComposerTransmitterReceiver
  dtkComposerTransmitterReceiver.h
  dtkComposerTransmitterReceiver.tpp
  dtkComposerView
  dtkComposerView.h
  dtkComposerReader
  dtkComposerReader.h
  dtkComposerReaderNoScene.h
  dtkComposerWidget.h
  dtkComposerWriter
  dtkComposerWriter.h
  dtkComposerWriterNoScene.h
  dtkComposerViewController
  dtkComposerViewController.h
  dtkComposerViewLayout
  dtkComposerViewLayout.h
  dtkComposerViewLayoutItem
  dtkComposerViewLayoutItem.h
  dtkComposerViewList
  dtkComposerViewList.h
  dtkComposerViewListControl
  dtkComposerViewListControl.h
  dtkComposerViewManager
  dtkComposerViewManager.h
  dtkComposerViewWidget.h)

set(${PROJECT_NAME}_SOURCES
  dtkComposer.cpp
  dtkComposerCompass.cpp
  dtkComposerControls.cpp
  dtkComposerControlsDelegate.cpp
  dtkComposerControlsListItem.cpp
  dtkComposerControlsListItemBoolean.cpp
  dtkComposerControlsListItemFactory.cpp
  dtkComposerControlsListItemFile.cpp
  dtkComposerControlsListItemInteger.cpp
  dtkComposerControlsListItemReal.cpp
  dtkComposerControlsListItemString.cpp
  dtkComposerDefaultExtensions.cpp
  dtkComposerEvaluator.cpp
  dtkComposerEvaluatorProcess.cpp
  dtkComposerEvaluatorSlave.cpp
  dtkComposerEvaluatorToolBar.cpp
  dtkComposerGraph.cpp
  dtkComposerGraphEdge.cpp
  dtkComposerGraphNodeBegin.cpp
  dtkComposerGraphNodeBeginForEach.cpp
  dtkComposerGraphNode.cpp
  dtkComposerGraphNodeEnd.cpp
  dtkComposerGraphNodeLeaf.cpp
  dtkComposerGraphNodeSelectBranch.cpp
  dtkComposerGraphNodeSetInputs.cpp
  dtkComposerGraphNodeSetOutputs.cpp
  dtkComposerGraphNodeSetVariables.cpp
  dtkComposerGraphView.cpp
  dtkComposerNodeGraph.cpp
  dtkComposerNodeGraphEdge.cpp
  dtkComposerNode.cpp
  dtkComposerNodeMetaData.cpp
  dtkComposerNodeFactory.cpp
  dtkComposerNodeFactoryView.cpp
  dtkComposerNodeBoolean.cpp
  dtkComposerNodeBooleanOperator.cpp
  dtkComposerNodeComposite.cpp
  dtkComposerNodeConstants.cpp
  dtkComposerNodeControl.cpp
  dtkComposerNodeControlCase.cpp
  dtkComposerNodeControlDoWhile.cpp
  dtkComposerNodeControlIf.cpp
  dtkComposerNodeControlFor.cpp
  dtkComposerNodeControlForEach.cpp
  dtkComposerNodeControlMap.cpp
  dtkComposerNodeControlParallelMap.cpp
  dtkComposerNodeControlWhile.cpp
  dtkComposerNodeDistributed.cpp
  dtkComposerNodeExec.cpp
  dtkComposerNodeFile.cpp
  dtkComposerNodeInteger.cpp
  dtkComposerNodeLeaf.cpp
  dtkComposerNodeRange.cpp
  dtkComposerNodeLogger.cpp
  dtkComposerNodeMetaContainer.cpp
  dtkComposerNodeNumberOperator.cpp
  dtkComposerNodePrint.cpp
  dtkComposerNodeProxy.cpp
  dtkComposerNodeReal.cpp
  dtkComposerNodeRemote.cpp
  dtkComposerNodeSpawn.cpp
  dtkComposerNodeSpawnEmbedded.cpp
  dtkComposerNodeString.cpp
  dtkComposerNodeStringOperator.cpp
  dtkComposerPath.cpp
  dtkComposerScene.cpp
  dtkComposerSceneEdge.cpp
  dtkComposerSceneModel.cpp
  dtkComposerSceneNode.cpp
  dtkComposerSceneNodeComposite.cpp
  dtkComposerSceneNodeControl.cpp
  dtkComposerSceneNodeHandle.cpp
  dtkComposerSceneNodeEditor.cpp
  dtkComposerSceneNodeLeaf.cpp
  dtkComposerSceneNote.cpp
  dtkComposerScenePort.cpp
  dtkComposerSceneView.cpp
  dtkComposerSearchDialog.cpp
  dtkComposerSettings.cpp
  dtkComposerStack.cpp
  dtkComposerStackCommand.cpp
  dtkComposerStackUtils.cpp
  dtkComposerStackView.cpp
  dtkComposerTransmitter.cpp
  dtkComposerTransmitterEmitter.cpp
  dtkComposerTransmitterReceiver.cpp
  dtkComposerTransmitterProxy.cpp
  dtkComposerTransmitterProxyLoop.cpp
  dtkComposerTransmitterProxyVariant.cpp
  dtkComposerView.cpp
  dtkComposerReader.cpp
  dtkComposerReaderNoScene.cpp
  dtkComposerWidget.cpp
  dtkComposerWriter.cpp
  dtkComposerWriterNoScene.cpp
  dtkComposerViewController.cpp
  dtkComposerViewLayout.cpp
  dtkComposerViewLayoutItem.cpp
  dtkComposerViewList.cpp
  dtkComposerViewListControl.cpp
  dtkComposerViewManager.cpp
  dtkComposerViewWidget.cpp)

set(${PROJECT_NAME}_RCC dtkComposer.qrc)

set_property(SOURCE qrc_dtkComposer.cpp PROPERTY SKIP_AUTOMOC ON)

## #################################################################
## Build rules
## #################################################################

qt5_add_resources(${PROJECT_NAME}_SOURCES_QRC ${${PROJECT_NAME}_RCC})

add_library(${PROJECT_NAME} SHARED
  ${${PROJECT_NAME}_SOURCES_QRC}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

set_target_properties(${PROJECT_NAME} PROPERTIES CXX_VISIBILITY_PRESET hidden)
set_target_properties(${PROJECT_NAME} PROPERTIES VISIBILITY_INLINES_HIDDEN 1)
set_target_properties(${PROJECT_NAME} PROPERTIES MACOSX_RPATH 0)
set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH    "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${dtk_VERSION}
  SOVERSION ${dtk_VERSION_MAJOR})

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} Qt5::Core)
target_link_libraries(${PROJECT_NAME} Qt5::Concurrent)
target_link_libraries(${PROJECT_NAME} Qt5::Gui)
target_link_libraries(${PROJECT_NAME} Qt5::Network)
target_link_libraries(${PROJECT_NAME} Qt5::Svg)
target_link_libraries(${PROJECT_NAME} Qt5::Widgets)
target_link_libraries(${PROJECT_NAME} Qt5::Xml)

target_link_libraries(${PROJECT_NAME} dtkLog)
target_link_libraries(${PROJECT_NAME} dtkCore)
target_link_libraries(${PROJECT_NAME} dtkMeta)
target_link_libraries(${PROJECT_NAME} dtkWidgets)
target_link_libraries(${PROJECT_NAME} dtkDistributed)
target_link_libraries(${PROJECT_NAME} dtkMath)

## #################################################################
## Export header file
## #################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export.h")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
 "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export"
 "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(FILES ${${PROJECT_NAME}_HEADERS}
  DESTINATION include/${PROJECT_NAME}
    COMPONENT composer)

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME}
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
      COMPONENT composer
         EXPORT dtkDepends)

######################################################################
### CMakeLists.txt ends here
