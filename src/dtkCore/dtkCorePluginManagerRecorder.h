// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCoreExport.h>

class QString;

class dtkCorePluginManagerBase;
class dtkCoreLayerManager;

// /////////////////////////////////////////////////////////////////
// dtkCorePluginManagerRecorder interface
// /////////////////////////////////////////////////////////////////

class DTKCORE_EXPORT dtkCorePluginManagerRecorder
{
public:
    explicit dtkCorePluginManagerRecorder(dtkCoreLayerManager *layer_manager, dtkCorePluginManagerBase *plugin_manager, const QString& plugin_manager_name);
            ~dtkCorePluginManagerRecorder(void) = default;
};

//
// dtkCorePluginManagerRecorder.h ends here
