// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include <Python.h>

#include "dtkScriptInterpreterPython.h"

#include <dtkLog/dtkLog.h>
#include <dtkLog/dtkLogger.h>

#include <QtCore>


// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

static const QString dtkScriptInterpreterPythonRedirector_declare =
    "import sys\n"
    "\n"
    "class Redirector:\n"
    "    def __init__(self):\n"
    "        self.data = ''\n"
    "    def write(self, stuff):\n"
    "        self.data+= stuff\n";

static const QString dtkScriptInterpreterPythonRedirector_define =
    "redirector = Redirector()\n"
    "sys.stdout = redirector\n"
    "sys.stderr = redirector\n";

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkScriptInterpreterPythonPrivate
{
public:
    QString buffer;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

dtkScriptInterpreterPython::dtkScriptInterpreterPython(QObject *parent) : dtkScriptInterpreter(parent), d(new dtkScriptInterpreterPythonPrivate)
{
    Py_Initialize();
    // PyRun_SimpleString(dtkScriptInterpreterPythonRedirector_declare.toUtf8().constData());

    QString paths;

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-script");
    settings.beginGroup("modules");
    paths = settings.value("path").toString();
    settings.endGroup();

    PyRun_SimpleString("import sys");

    foreach(QString path, paths.split(":", QString::SkipEmptyParts))
        PyRun_SimpleString(QString("sys.path.append('%1')").arg(path).toUtf8().constData());

}

dtkScriptInterpreterPython::~dtkScriptInterpreterPython(void)
{
    Py_Finalize();

    delete d;

    d = NULL;
}

void dtkScriptInterpreterPython::init(void)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-script");
    settings.beginGroup("init");
    QString init = settings.value("script").toString();
    settings.endGroup();

    if (!init.isEmpty()) {
        PyRun_SimpleString(QString("execfile('%1')").arg(init).toUtf8().constData());
    } else {
        dtkWarn() << "no init function " ;
    }
}

QString dtkScriptInterpreterPython::interpret(const QString& command, int *stat)
{
    QString statement = command;

    if (command.endsWith(":")) {
        if(!d->buffer.isEmpty())
            d->buffer.append("\n");

        d->buffer.append(command);

        return "";
    }

    if (!command.isEmpty() && command.startsWith(" ")) {
        if(!d->buffer.isEmpty())
            d->buffer.append("\n");

        d->buffer.append(command);

        return "";
    }

    if (command.isEmpty() && !d->buffer.isEmpty()) {
        if(!d->buffer.isEmpty())
            d->buffer.append("\n");

        statement = d->buffer;

        d->buffer.clear();
    }

    if (statement.isEmpty())
        return "";

    PyObject *module = PyImport_AddModule("__main__");

    // PyRun_SimpleString(dtkScriptInterpreterPythonRedirector_define.toUtf8().constData());

    switch (PyRun_SimpleString(statement.toUtf8().constData())) {
    case  0: *stat = Status_Ok;    break;
    case -1: *stat = Status_Error; break;
    default: break;
    }

    PyErr_Print();

    // PyObject *redtor = PyObject_GetAttrString(module, "redirector");
    // PyObject *result = PyObject_GetAttrString(redtor, "data");

    // return QString(PyString_AsString(result)).simplified();

    return QString();
}

//
// dtkScriptInterpreterPython.cpp ends here
