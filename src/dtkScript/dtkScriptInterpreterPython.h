// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkScriptInterpreter.h"

class dtkScriptInterpreterPythonPrivate;

class dtkScriptInterpreterPython : public dtkScriptInterpreter
{
    Q_OBJECT

public:
    DTKSCRIPT_EXPORT  dtkScriptInterpreterPython(QObject *parent = 0);
    DTKSCRIPT_EXPORT ~dtkScriptInterpreterPython(void);

public slots:
    DTKSCRIPT_EXPORT QString interpret(const QString& command, int *stat);
    DTKSCRIPT_EXPORT void init(void);

private:
    dtkScriptInterpreterPythonPrivate *d;
};

//
// dtkScriptInterpreterPython.h ends here
