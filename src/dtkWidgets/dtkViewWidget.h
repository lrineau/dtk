// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport.h>

#include <QtWidgets>

class DTKWIDGETS_EXPORT dtkViewWidget : public QWidget
{
    Q_OBJECT

public:
     dtkViewWidget(QWidget *parent = Q_NULLPTR);
    ~dtkViewWidget(void);

signals:
    void   focused(void);
    void unfocused(void);

public:
    virtual QWidget *widget(void);
    virtual QWidget *inspector(void);

protected:
    void mousePressEvent(QMouseEvent *);

private:
    class dtkViewWidgetPrivate *d;
};

//
// dtkViewWidget.h ends here
