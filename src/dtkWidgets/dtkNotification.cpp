// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNotification.h"
#include "dtkNotificationQueue.h"
#include "dtkNotificationEvent.h"

#include <QtWidgets>

void dtkNotify(const QString& message)
{
    dtkNotificationEvent *event = new dtkNotificationEvent;
    event->setType(dtkNotificationEvent::Persistent);
    event->setMessage(message);

    dtkNotify(event);
}

void dtkNotify(const QString& message, int duration)
{
    dtkNotificationEvent *event = new dtkNotificationEvent;
    event->setType(dtkNotificationEvent::NonPersistent);
    event->setMessage(message);
    event->setDuration(duration);

    dtkNotify(event);
}

void dtkNotify(dtkNotificationEvent *event)
{
    QApplication::postEvent(dtkNotificationQueue::instance(), event);
}

void dtkClearNotifications(void)
{
    dtkNotificationQueue::instance()->clear();
}

//
// dtkNotification.cpp ends here
